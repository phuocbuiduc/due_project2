import 'package:due_project/src/firebase/Firebase_Auth.dart';
import 'package:due_project/src/resource/SplashScreen.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    theme: ThemeData(
        primaryColor: Colors.lightBlue, accentColor: Colors.yellowAccent),
    debugShowCheckedModeBanner: false,
    home: SplashScreen(auth: new FirAuth()),
  ));
}
