import 'package:due_project/src/models/Notifications.dart';


import 'IntToDateTime.dart';

class SortListNotificationByDateTime {
  static List<Notifications> sortListNotificationByDateTime(
      List<Notifications> list) {
    list.sort((a, b) {
      return IntToDateTime.convertIntToDateTime(b.time)
          .compareTo(IntToDateTime.convertIntToDateTime(a.time));
    });

    return list;
  }
}
