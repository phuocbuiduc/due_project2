import 'package:due_project/src/models/Users.dart';

class UserToListTopic {
  static List<String> convertToListTopic(Users user) {

    final String subStringK = user.nclass.split("k")[0] + "k";

    List<String> listTopics = [];
    listTopics.add("ALL");
    listTopics.add(user.mssv);
    listTopics.add(user.khoaID);
    listTopics.add(user.nclass);
    listTopics.add(user.sex);
    listTopics.add(subStringK);
    
    return listTopics;
  }
}
