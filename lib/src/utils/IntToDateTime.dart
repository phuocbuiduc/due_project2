import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class IntToDateTime {
  static String convertIntToDateTime(String timestamp) {
    var date;
    String formattedDate;
    try {
      date = new DateTime.fromMillisecondsSinceEpoch(int.parse(timestamp));
       formattedDate = DateFormat('kk:mm dd/MM').format(date);
    } on Exception catch (_) {
      return "";
    }
    return formattedDate;
  }
}
