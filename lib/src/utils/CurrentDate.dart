import 'package:due_project/src/models/Users.dart';
import 'package:intl/intl.dart';

class CurrentDate {
  static String getCurrentDate() {

    var dt = DateTime.now();

    var newDt = DateFormat.yMMMEd().format(dt);

    return newDt.toString();
  }
}