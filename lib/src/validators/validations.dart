
class Validations {
  static bool isValidUser(String user) {
    return user != null && user.length > 6 && user.contains("@");
  }

  static bool isValidPass(String pass){
    return pass != null && pass.length >= 6;
  }

  static bool isValidEmail(String email){
    return email != null && email.length >= 1 && RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(email);
  }

}