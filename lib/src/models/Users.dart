import 'package:firebase_database/firebase_database.dart';

class Users {
  String key;
  String userId;
  String nclass;
  String email;
  String khoaID;
  String khoaName;
  String mssv;
  String name;
  String phone;
  String sex;


  @override
  String toString() {
    return 'Users{key: $key, userId: $userId, nclass: $nclass, email: $email, khoaId: $khoaID, khoaName: $khoaName, mssv: $mssv, name: $name, phone: $phone, sex: $sex}';
  }

  Users(this.userId, this.nclass, this.email, this.khoaID, this.khoaName,
      this.mssv, this.name, this.phone, this.sex);

  Users.fromSnapshot(DataSnapshot snapshot)
      : key = snapshot.key,
        userId = snapshot.key,
        nclass = snapshot.value['nclass'],
        email = snapshot.value["email"],
        khoaID = snapshot.value["khoaID"],
        khoaName = snapshot.value["khoaName"],
        mssv = snapshot.value["mssv"],
        name = snapshot.value["name"],
        phone = snapshot.value["phone"],
        sex = snapshot.value["sex"];

  toJson() {
    return {
      "userId": userId,
      "nclass": nclass,
      "email": email,
      "khoaID": khoaID,
      "khoaName": khoaName,
      "mssv": mssv,
      "name": name,
      "phone": phone,
      "sex": sex,
    };
  }
}
