

class Khoa {
  String id;
  String name;

  Khoa(this.id, this.name);

  static List<Khoa> getKhoa() {
    return <Khoa>[
      Khoa("khoaketoan", 'Kế toán'),
      Khoa("khoamarketing", 'Marketing'),
      Khoa("khoaquantrikinhdoanh", 'Quản trị kinh doanh'),
      Khoa("khoadulich", 'Du lịch'),
      Khoa("khoakinhdoanhquocte", 'Kinh doanh quốc tế'),
      Khoa("khoanganhang", 'Ngân hàng'),
      Khoa("khoataichinh", 'Tài chính'),
      Khoa("khoathongketinhoc", 'Thống kê - Tin học'),
      Khoa("khoaluat", 'Luật'),
      Khoa("khoakinhte", 'Kinh Tế'),
      Khoa("khoathuongmaidientu", 'Thương mại điện tử'),
      Khoa("khoalyluanchinhtri", 'Lý luận chính trị'),
    ];
  }
}
