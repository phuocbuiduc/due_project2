import 'package:due_project/src/firebase/Firebase_Auth.dart';
import 'package:due_project/src/resource/SplashScreen.dart';
import 'package:due_project/src/utils/ConvertUserToListTopics.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/scheduler.dart';
import 'Users.dart';

class UserFirebase extends StatefulWidget {
  const UserFirebase({Key key, this.userID, this.loginCallback, this.auth})
      : super(key: key);
  final String userID;
  final VoidCallback loginCallback;
  final FirAuth auth;

  @override
  _UserFirebaseState createState() => _UserFirebaseState();
}

class _UserFirebaseState extends State<UserFirebase> {
  Users user;
  DatabaseReference userRef;
  final databaseReference = FirebaseDatabase.instance.reference();

  final FirebaseMessaging _fcm = FirebaseMessaging();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    userRef = databaseReference.child('users').child(widget.userID);

    Future<Users> users = getUserInfo();

    // ignore: unnecessary_statements
    widget.loginCallback;

    SchedulerBinding.instance.addPostFrameCallback((_) {
      users.then((userFireBase) {
        print("USER GET FIREBASE: " + userFireBase.toString());

        Navigator.of(context).pushReplacement(MaterialPageRoute(
            builder: (context) => SplashScreen(
                  auth: widget.auth,
                  userFireBase: userFireBase,
                )));
      });
    });
  }

  Future<Users> getUserInfo() async {
    await userRef.once().then((DataSnapshot snapshot) {
      print("Sau Login: " + snapshot.value.toString());

      user = Users.fromSnapshot(snapshot);

      print("user tai: UserFir" + user.toString());
      print("listTOPICS: " + UserToListTopic.convertToListTopic(user).toString());

      List<String> listTopics = UserToListTopic.convertToListTopic(user);

      listTopics.forEach((topics) {
        _fcm.subscribeToTopic(topics).then((done) {
          print("subcribed done: " + topics);
        });
      });
    });

    return user;
  }

  Widget _getData(DataSnapshot snapshot) {
    user = Users.fromSnapshot(snapshot);

    return Container(
      child: Column(
        children: <Widget>[
          Text("UserID: " + user.userId),
          Text("Name: " + user.name),
          Text("Giới Tính: " + user.sex),
          Text("Lớp: " + user.nclass),
          Text("Khoa: " + user.khoaName),
          Text("Mã Số SV: " + user.mssv),
          Text("Email: " + user.email),
          Text("Số DT: " + user.phone),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                  image: new ExactAssetImage('assets/dhkt.png'),
                  colorFilter: ColorFilter.mode(
                      Colors.black.withOpacity(0.8), BlendMode.dstATop),
                  fit: BoxFit.cover),
            ),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Expanded(
                flex: 1,
                child: Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[],
                  ),
                ),
              ),
              Expanded(
                flex: 2,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    CircularProgressIndicator(),
                    Padding(
                      padding: EdgeInsets.only(top: 60.0),
                    ),
                  ],
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}
