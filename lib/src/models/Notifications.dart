import 'package:firebase_database/firebase_database.dart';

class Notifications {
  String name;
  String content;
  String time;
  String title;
  String topic;


  Notifications(this.name, this.content, this.time, this.title, this.topic);


  @override
  String toString() {
    return 'Notifications{name: $name, content: $content, time: $time, title: $title, topic: $topic}';
  }

  Notifications.fromJson(Map<dynamic, dynamic> data):
    title = data['title'],
    content = data["content"],
    name = data["name"],
    time = data["time"].toString(),
    topic = data["topic"];


  Notifications.fromSnapshot(DataSnapshot snapshot)
      : title = snapshot.value['title'],
        content = snapshot.value["content"],
        name = snapshot.value["name"],
        time = snapshot.value["time"],
        topic = snapshot.value["topic"];

  toJson() {
    return {
      "title": title,
      "content": content,
      "name": name,
      "time": time,
      "topic": topic,
    };
  }
}
