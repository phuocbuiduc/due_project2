import 'package:flutter/material.dart';

class Destination {
  const Destination(this.title, this.icon, this.color);
  final String title;
  final IconData icon;
  final MaterialColor color;
}

const List<Destination> allDestinations = <Destination>[
  Destination('Home', Icons.home, Colors.lightBlue),
  Destination('News', Icons.school, Colors.lightBlue),
  Destination('Notification', Icons.notifications_active, Colors.lightBlue),
  Destination('Calendar', Icons.today  , Colors.lightBlue)
];