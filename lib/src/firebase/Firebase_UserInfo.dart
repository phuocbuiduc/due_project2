import 'package:due_project/src/models/Users.dart';
import 'dart:async';

import 'package:firebase_database/firebase_database.dart';


class FirUser {
  Users user;
  DatabaseReference userRef;
  final databaseReference = FirebaseDatabase.instance.reference();

  Future<Users> getCurrentUser(userID) async {
    userRef = databaseReference.child('users').child(userID);

    await userRef.once().then((DataSnapshot snapshot) {
      print("=====CALL AGAIN USERINFO=====////: " + snapshot.value.toString());

      user = Users.fromSnapshot(snapshot);
      return user;
    });

    return user;
  }
}
