import 'package:due_project/src/models/Khoa.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/services.dart';
import 'dart:async';

class FirAuth {
  FirebaseAuth _firebaseAuth = FirebaseAuth.instance;

  //function register account
  Future<void> signUp(
      String email,
      String pass,
      String name,
      String phone,
      String nclass,
      String stcode,
      Khoa khoa,
      int sex,
      Function onSuccess,
      Function(String) onRegisterError) async {
    await _firebaseAuth
        .createUserWithEmailAndPassword(email: email, password: pass)
        .then((user) {
      _createUser(user.user.uid, name, phone, email, nclass, stcode, khoa, sex,
          onSuccess, onRegisterError);
      print("user: tai FirAuth " + user.toString());
      sendEmailVerification();
    }).catchError((err) {
      PlatformException error = err;
      print("check error code:" + error.code);

      _onSignUpErr(error.code, onRegisterError);

      // TODO
    });
  }

  Future<FirebaseUser> getCurrentUser() async {
    FirebaseUser user = await _firebaseAuth.currentUser();
    return user;
  }

  Future<void> signOut() async {
    return _firebaseAuth.signOut();
  }

  Future<void> sendEmailVerification() async {
    print("SEND EMAIL VERIFY: ");
    FirebaseUser user = await _firebaseAuth.currentUser();
    user.sendEmailVerification();
  }

  Future<bool> isEmailVerified() async {
    FirebaseUser user = await _firebaseAuth.currentUser();
    return user.isEmailVerified;
  }

  //create function login
  Future<void> signIn(String email, String pass, Function onSuccess,
      Function(String) onSignInError) async {
    bool checkError = false;
    AuthResult result = await _firebaseAuth
        .signInWithEmailAndPassword(email: email, password: pass)
        .catchError((err) {
      checkError = true;
      onSignInError("Đăng nhập thất bại, Vui lòng thử lại");
    });
    if(!checkError){
      onSuccess();
    }
  }

  // Create more information about UserId on realtime DB Firebase
  _createUser(
      String userID,
      String name,
      String phone,
      email,
      String nclass,
      String stcode,
      Khoa khoa,
      int sex,
      Function onSuccess,
      Function(String) onRegisterError) {
    var user = {
      "name": name,
      "phone": phone,
      "email": email,
      "nclass": nclass,
      "mssv": stcode,
      "khoaID": khoa.id,
      "khoaName": khoa.name,
      "sex": sex == 0 ? "Nu" : "Nam"
    };
    var ref = FirebaseDatabase.instance.reference().child("users");
    ref.child(userID).set(user).then((user) {
      // Succesful
      onSuccess();
    }).catchError((err) {
      // TODO
      onRegisterError("Đăng ký thất bại, Vui lòng thử lại sau.");
    });
  }

  ///  * `ERROR_WEAK_PASSWORD` - If the password is not strong enough.
  ///  * `ERROR_INVALID_EMAIL` - If the email address is malformed.
  ///  * `ERROR_EMAIL_ALREADY_IN_USE` - If the email is already in use by a different account.
  void _onSignUpErr(String code, Function(String) onRegisterError) {
    switch (code) {
      case "ERROR_INVALID_EMAIL":
      case "ERROR_INVALID_CREDENTIAL":
        onRegisterError("Email không hợp lệ.");
        break;
      case "ERROR_EMAIL_ALREADY_IN_USE":
        onRegisterError("Email này đã tồn tại.");
        break;
      case "ERROR_WEAK_PASSWORD":
        onRegisterError("Mật khẩu không đủ mạnh.");
        break;
      default:
        onRegisterError("Đăng Ký thất bại vui lòng thử lại");
        break;
    }
  }

  Future<void> resetPassword(String email) async {
    print("RESET PASS WORD");
    await _firebaseAuth.sendPasswordResetEmail(email: email);
  }
}
