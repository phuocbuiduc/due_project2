import 'dart:async';

import 'package:due_project/src/firebase/Firebase_Auth.dart';
import 'package:due_project/src/models/Khoa.dart';
import 'package:due_project/src/validators/validations.dart';

class AuthBloc {
  var _firAuth = FirAuth();

  StreamController _nameController = new StreamController();
  StreamController _emailController = new StreamController();
  StreamController _passController = new StreamController();
  StreamController _phoneController = new StreamController();
  StreamController _stcodeController = new StreamController();
  StreamController _classController = new StreamController();

  Stream get nameStream => _nameController.stream;

  Stream get emailStream => _emailController.stream;

  Stream get passStream => _passController.stream;

  Stream get phoneStream => _phoneController.stream;

  Stream get stcodeStream => _stcodeController.stream;

  Stream get classStream => _classController.stream;

  bool isValid(String name, String email, String pass, String phone,
      String nclass, String stcode) {
    if (name == null || name.length < 6) {
      _nameController.sink.addError("Tên phải lớn hơn 6 kí tự!");
      return false;
    }
    _nameController.sink.add("");

    String pattern = r'(^(?:[+0]9)?[0-9]{10,12}$)';
    RegExp regExp = new RegExp(pattern);
    if (phone == null || phone.length == 0) {
      _phoneController.sink.addError("Nhập số điện thoại");

      return false;
    } else if (!regExp.hasMatch(phone)) {
      _phoneController.sink.addError('Vui lòng nhập đúng định dạng số ĐT');
      return false;
    }
    _phoneController.sink.add("");

    if (stcode == null || stcode.length == 0) {
      _stcodeController.sink.addError("Nhập mã số sinh viên");
      return false;
    }
    _stcodeController.sink.add("");

    nclass = nclass.toLowerCase();
    if (nclass == null || nclass.length == 0 || !nclass.contains("k")) {
      _classController.sink.addError("Nhập Tên Lớp Định dạng: 42K21");
      return false;
    }
    _classController.sink.add("");

    if (!Validations.isValidEmail(email)) {
      _emailController.sink.addError("Nhập email đúng định dạng");
      return false;
    }
    _emailController.sink.add("");

    if (pass == null || pass.length < 6) {
      _passController.sink.addError("Mật khẩu phải trên 5 ký tự");
      return false;
    }
    _passController.sink.add("");

    return true;
  }

  void signUp(
      String email,
      String pass,
      String name,
      String phone,
      String nclass,
      String stcode,
      Khoa khoa,
      int sex,
      Function onSuccess,
      Function(String) onRegisterError) {
    _firAuth.signUp(email, pass, name, phone, nclass, stcode, khoa, sex,
        onSuccess, onRegisterError);
  }

  void dispose() {
    _nameController.close();
    _emailController.close();
    _passController.close();
    _phoneController.close();
    _stcodeController.close();
    _classController.close();
  }
}
