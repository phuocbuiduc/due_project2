import 'dart:async';

import 'package:due_project/src/firebase/Firebase_Auth.dart';
import 'package:due_project/src/validators/validations.dart';

class LoginBloc {
  StreamController _userController = new StreamController();
  StreamController _passController = new StreamController();

  Stream get userStream => _userController.stream;

  Stream get passStream => _passController.stream;


  bool isValidInfo(String username, String pass) {
    if (!Validations.isValidEmail(username)) {
      _userController.sink.addError("Email không hợp lệ!");
      return false;
    }
    _userController.sink.add("OK");
    if (!Validations.isValidPass(pass)) {
      _passController.sink.addError("Mật khẩu phải trên 6 ký tự!");
      return false;
    }

    _passController.sink.add("OK");

    return true;
  }

  Future<void> signIn(FirAuth auth, String email, String pass,
      Function onSuccess, Function(String) onSignInError) async{
     await auth.signIn(email, pass, onSuccess, onSignInError);
  }

  void dispose() {
    _userController.close();
    _passController.close();
  }
}
