import 'dart:async';

import 'package:due_project/src/firebase/Firebase_Auth.dart';
import 'package:due_project/src/validators/validations.dart';

class ResetEmailBloc {
  // ignore: close_sinks
  StreamController _resetEmailController = new StreamController.broadcast();

  Stream get resetEmailStream => _resetEmailController.stream;

  bool isValidInfo(String email) {
    if (!Validations.isValidEmail(email)) {
      _resetEmailController.sink.addError("Email Không hợp lệ!");
      return false;
    }
    _resetEmailController.sink.add("OK");

    return true;
  }

  Future<void> resetPassword(FirAuth auth, String email) async {
    return await auth.resetPassword(email);
  }

  void dispose() {
    _resetEmailController.close();
  }
}
