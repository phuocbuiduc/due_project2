import 'package:flutter/material.dart';

class NotificationsDetails extends StatefulWidget {
  final String time;
  final String message;
  final String name;

  const NotificationsDetails({Key key, this.time, this.message, this.name})
      : super(key: key);

  @override
  _NotificationsDetailsState createState() => _NotificationsDetailsState();
}

class _NotificationsDetailsState extends State<NotificationsDetails> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: const Text('Thông Báo',
            style: TextStyle(
              fontSize: 20,
            )),
        actions: [
          new FlatButton(
              onPressed: () {
                //TODO: Handle save
              },
              child: null),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 5),
                child: Row(
                  children: [
                    Text(
                      "Người Gửi: ",
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.w700),
                    ),
                    Expanded(
                      child: Text(
                        widget.name,
                        style: TextStyle(fontSize: 19),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 10),
                child: Row(
                  children: [
                    Text("Thời Gian: ",
                        style: TextStyle(
                            fontSize: 18, fontWeight: FontWeight.w700)),
                    Text(widget.time,
                        style: TextStyle(
                            fontSize: 18, fontStyle: FontStyle.italic)),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 10),
                child: Center(
                  child: Text(
                    "Nội Dung:",
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.w700),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
                child: Container(
                    alignment: Alignment.topLeft,
                    child:
                        Text(widget.message, style: TextStyle(fontSize: 18))),
              )
            ],
          ),
        ),
      ),
    );
  }
}
