import 'dart:async';

import 'package:due_project/src/firebase/Firebase_Auth.dart';
import 'package:due_project/src/firebase/Firebase_UserInfo.dart';
import 'package:due_project/src/models/Destinations/Destination.dart';
import 'package:due_project/src/models/Users.dart';
import 'package:flutter/material.dart';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/scheduler.dart';

import 'DueWebView.dart';
import 'HomeViewDemo.dart';
import 'ListNotificationView.dart';
import 'TableCalendarView.dart';

class HomeView extends StatefulWidget {
  HomeView(
      {Key key,
      this.auth,
      this.userId,
      this.logoutCallback,
      this.fcm,
      this.userFirebase})
      : super(key: key);

  final FirAuth auth;
  final VoidCallback logoutCallback;
  final String userId;
  final FirebaseMessaging fcm;
  final Users userFirebase;

  @override
  _HomeViewState createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView>
    with TickerProviderStateMixin<HomeView> {
  int _currentIndex = 0;
  Users userRenew;
  FirUser firUser = new FirUser();

  Future<void> _getDeviceToken() async {
    // Get the current user
    String uid = widget.userId;
    // FirebaseUser user = await _auth.currentUser();

    // Get the token for this device
    String fcmToken = await widget.fcm.getToken();

    print('=====DEVICE TOKEN====/////:' + fcmToken);
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    userRenew = widget.userFirebase;

    if (widget.userFirebase == null) {
      firUser.getCurrentUser(widget.userId).then((user) {
        setState(() {
          userRenew = user;
        });
        print("====SAU KHI GET AGAIN USERINFO===/// " + user.toString());
      });
    }

    _getDeviceToken();
  }

  @override
  Widget build(BuildContext context) {
    if (userRenew != null) {
      return Scaffold(
          body: SafeArea(
            top: false,
            child: IndexedStack(
              index: _currentIndex,
              children: <Widget>[
                HomeViewDemo(
                  auth: widget.auth,
                  logoutCallback: widget.logoutCallback,
                  userId: widget.userId,
                  userFirebase: userRenew,
                ),
                DueWebView(
                  websiteName: "Đại Học Kinh Tế",
                  websiteUrl: "",
                ),
                ListNotificationView(
                  userFirebase: userRenew,
                  userID: widget.userId,
                ),
                TableCalendarView(
                  title: "SCHEDULE",
                ),
              ],
            ),
          ),
          bottomNavigationBar: BottomNavigationBar(
            currentIndex: _currentIndex,
            onTap: (int index) {
              setState(() {
                _currentIndex = index;
              });
            },
            items: allDestinations.map((Destination destination) {
              return BottomNavigationBarItem(
                  icon: Icon(destination.icon),
                  backgroundColor: destination.color,
                  title: Text(destination.title));
            }).toList(),
          ));
    } else {
      return Scaffold(
        body: Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                    image: new ExactAssetImage('assets/dhkt.png'),
                    colorFilter: ColorFilter.mode(
                        Colors.black.withOpacity(0.8), BlendMode.dstATop),
                    fit: BoxFit.cover),
              ),
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Container(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[],
                    ),
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      CircularProgressIndicator(),
                      Padding(
                        padding: EdgeInsets.only(top: 60.0),
                      ),
                    ],
                  ),
                )
              ],
            )
          ],
        ),
      );
    }
  }

  @override
  void dispose() {
    super.dispose();
  }
}
