import 'package:due_project/src/firebase/Firebase_Auth.dart';
import 'package:due_project/src/firebase/Firebase_UserInfo.dart';
import 'package:due_project/src/models/Users.dart';
import 'package:due_project/src/utils/CurrentDate.dart';
import 'package:flutter/material.dart';
import 'dart:math' as math;

import 'drawer/DrawerLeft.dart';

class HomeViewDemo extends StatefulWidget {
  const HomeViewDemo(
      {Key key, this.auth, this.logoutCallback, this.userId, this.userFirebase})
      : super(key: key);

  final FirAuth auth;
  final VoidCallback logoutCallback;
  final String userId;
  final Users userFirebase;

  @override
  _HomeViewDemoState createState() => _HomeViewDemoState();
}

class _HomeViewDemoState extends State<HomeViewDemo> {
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey();
  final GlobalKey<AnimatedListState> _listKey = GlobalKey<AnimatedListState>();
  ListModel listModel;
  bool showOnlyCompleted = false;

  static const double _imageHeight = 256.0;

  Users userRenew;
  FirUser firUser = new FirUser();

  @override
  void initState() {
    super.initState();

    userRenew = widget.userFirebase;

    if (widget.userFirebase == null) {
      firUser.getCurrentUser(widget.userId).then((user) {
        userRenew = user;
        print(
            "====NEU USERINFO NULL AT HOMEVIEWDEMO====/// " + user.toString());
      });
    }

    listModel = ListModel(_listKey, getListInfo(userRenew));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: Stack(
        children: <Widget>[
          _buildTimeline(),
          _buildImage(),
          _buildTopHeader(_scaffoldKey),
          _buildProfileRow(),
          _buildBottomPart(),
          _buildFab()
        ],
      ),
      drawer: DrawerLeft(
        auth: widget.auth,
        logoutCallback: widget.logoutCallback,
        userId: widget.userId,
        userFirebase: userRenew,
      ),
    );
  }

  Widget _buildTopHeader(_scaffoldKey) => Padding(
      padding: const EdgeInsets.symmetric(horizontal: 0, vertical: 24.0),
      child: Container(
        alignment: Alignment.topRight,
        child: IconButton(
          onPressed: () {
            _scaffoldKey.currentState.openDrawer();
          },
          icon: Icon(
            Icons.menu,
            color: Colors.white,
          ),
        ),
      ));

  Widget _buildImage() => ClipPath(
        clipper: DialogonalClipper(),
        child: Container(
          width: double.infinity,
          child: Image.asset(
            'assets/dhkt.png',
            fit: BoxFit.cover,
            height: _imageHeight,
            colorBlendMode: BlendMode.srcOver,
            color: Color.fromARGB(120, 20, 10, 40),
          ),
        ),
      );

  Widget _buildProfileRow() => Padding(
        padding: const EdgeInsets.only(top: _imageHeight / 2.5, left: 16.0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            CircleAvatar(
              minRadius: 32.0,
              maxRadius: 32.0,
              backgroundImage: AssetImage('assets/anh.jpg'),
            ),
            Expanded(
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 16.0, vertical: 0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text(
                      userRenew.name,
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 18.0,
                          fontWeight: FontWeight.w500),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 8.0),
                      child: Text(
                        "Khoa: "+ userRenew.khoaName,
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 15.0,
                            fontWeight: FontWeight.w400),
                        overflow: TextOverflow.ellipsis,
                        maxLines: 1,
                      ),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      );

  Widget _buildBottomPart() => Padding(
        padding: const EdgeInsets.only(top: _imageHeight),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[_buildMyTaskHeader(), _buildTaskList()],
        ),
      );

  Widget _buildMyTaskHeader() => Padding(
        padding: const EdgeInsets.fromLTRB(64.0, 0, 0, 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              'Thông Tin Của Bạn:',
              style: TextStyle(fontSize: 26.0, fontWeight: FontWeight.w400),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: Text(
                CurrentDate.getCurrentDate(),
                style: TextStyle(fontSize: 12.0, fontWeight: FontWeight.w300),
              ),
            )
          ],
        ),
      );

  Widget _buildTimeline() => Positioned(
        top: 0.0,
        bottom: 0.0,
        left: 32.0,
        child: Container(
          width: 1.0,
          color: Colors.grey[300],
        ),
      );

  Widget _buildTaskList() => Expanded(
      child: AnimatedList(
          key: _listKey,
          initialItemCount: getListInfo(userRenew).length,
          itemBuilder: (context, index, animation) => TaskRow(
                task: listModel[index],
                animation: animation,
              )));

  Widget _buildFab() => Positioned(
        top: _imageHeight - 100.0,
        right: -40.0,
        child: AnimatedFab(
          onClick: _changeFilterState,
        ),
      );

  _changeFilterState() {
    showOnlyCompleted = !showOnlyCompleted;
    getListInfo(userRenew).where((task) => !task.completed).forEach((task) {
      if (showOnlyCompleted) {
      } else {}
    });
  }
}

class TaskRow extends StatelessWidget {
  final Animation<double> animation;
  final Task task;

  const TaskRow({Key key, this.task, this.animation}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FadeTransition(
      opacity: animation,
      child: SizeTransition(
        sizeFactor: animation,
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 16.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 26.5),
                child: Container(
                  width: 12.0,
                  height: 12.0,
                  decoration:
                      BoxDecoration(shape: BoxShape.circle, color: task.color),
                ),
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      task.name,
                      style: TextStyle(fontSize: 18.0),
                    )
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10.0),
                child: Text(
                  task.time,
                  style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w300),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class AnimatedFab extends StatefulWidget {
  final VoidCallback onClick;

  const AnimatedFab({Key key, this.onClick}) : super(key: key);

  @override
  _AnimatedFabState createState() => _AnimatedFabState();
}

class _AnimatedFabState extends State<AnimatedFab>
    with SingleTickerProviderStateMixin {
  AnimationController _animationController;
  Animation<Color> _colorAnimation;
  final double expandedSize = 180.0;
  final hiddenSize = 20.0;

  @override
  void initState() {
    super.initState();
    _animationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 200));
    _colorAnimation =
        ColorTween(begin: Colors.orangeAccent, end: Colors.orange[800])
            .animate(_animationController);
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: expandedSize,
      height: expandedSize,
      child: AnimatedBuilder(
        animation: _animationController,
        builder: (context, child) => Stack(
          alignment: Alignment.center,
          children: <Widget>[
            _buildExpandedBackground(),
            _buildOption(Icons.check_circle, 0.0),
            _buildOption(Icons.flash_on, -math.pi / 3),
            _buildOption(Icons.access_time, -2 * math.pi / 3),
            _buildOption(Icons.error_outline, math.pi),
            _buildFabCore(),
          ],
        ),
      ),
    );
  }

  Widget _buildExpandedBackground() {
    double size =
        hiddenSize + (expandedSize - hiddenSize) * _animationController.value;
    return Container(
      height: size,
      width: size,
      decoration: BoxDecoration(shape: BoxShape.circle, color: Colors.blue),
    );
  }

  Widget _buildFabCore() {
    double scaleFactor = 2 * (_animationController.value - 0.5).abs();
    return FloatingActionButton(
      onPressed: _onTabTap,
      backgroundColor: _colorAnimation.value,
      child: Transform(
          alignment: Alignment.center,
          transform: Matrix4.identity()..scale(1.0, scaleFactor),
          child: Icon(
            _animationController.value > 0.5 ? Icons.close : Icons.filter_list,
            color: Colors.white,
            size: 26.0,
          )),
    );
  }

  Widget _buildOption(IconData icon, double angle) {
    double iconSize = 0.0;

    if (_animationController.value > 0.8) {
      iconSize = 26.0 * (_animationController.value - 0.8) * 5;
    }

    return Transform.rotate(
      angle: angle,
      child: Align(
        alignment: Alignment.topCenter,
        child: Padding(
          padding: const EdgeInsets.only(top: 8.0),
          child: IconButton(
            onPressed: _onIconClick,
            icon: Transform.rotate(
              angle: -angle,
              child: Icon(
                icon,
                color: Colors.white,
              ),
            ),
            iconSize: iconSize,
            alignment: Alignment.center,
            padding: EdgeInsets.all(0.0),
          ),
        ),
      ),
    );
  }

  _onTabTap() {
    if (_animationController.isDismissed) {
      open();
    } else {
      close();
    }
  }

  _onIconClick() {
    widget.onClick();
    close();
  }

  open() {
    if (_animationController.isDismissed) {
      _animationController.forward();
    }
  }

  close() {
    if (_animationController.isCompleted) {
      _animationController.reverse();
    }
  }
}

class DialogonalClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) => Path()
    ..lineTo(0.0, size.height - 60.0)
    ..lineTo(size.width, size.height)
    ..lineTo(size.width, 0.0)
    ..close();

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => true;
}

class Task {
  final String name;
  final String category;
  final String time;
  final Color color;
  final bool completed;

  Task({this.name, this.category, this.time, this.color, this.completed});
}

class ListModel {
  final GlobalKey<AnimatedListState> listKey;
  final List<Task> items;

  ListModel(this.listKey, items) : this.items = List.of(items);

  AnimatedListState get _animatedList => listKey.currentState;

  int get length => items.length;

  Task operator [](int index) => items[index];

  int indexOf(Task item) => items.indexOf(item);
}

List<Task> getListInfo(Users user) {
  List<Task> getTask = [
    Task(
        name: "Tổng Số Tín Chỉ Tích Lũy: ",
        category: "",
        time: "116/120",
        color: Colors.orange,
        completed: false),
    Task(
        name: "Điểm Tích Lũy Toàn Khóa: ",
        category: "Email:",
        time: " 3.5 /4.0",
        color: Colors.cyan,
        completed: true),
    Task(
        name: "Điểm Ngoại Khóa: ",
        category: "Email:",
        time: " 90",
        color: Colors.red,
        completed: true),
    Task(
        name: "Điểm Rèn Luyện: ",
        category: "Email:",
        time: " 100 Điểm",
        color: Colors.lightGreenAccent,
        completed: true),
    Task(
        name: "Chứng Chỉ ANQP: ",
        category: "Email:",
        time: " Đã có",
        color: Colors.brown,
        completed: true),
    Task(
        name: "Chứng Chỉ THVP: ",
        category: "Email:",
        time: " Chưa",
        color: Colors.grey,
        completed: true),
    Task(
        name: "Chứng Chỉ Tiếng Anh: ",
        category: "Email:",
        time: " PTE 30+",
        color: Colors.orange,
        completed: true),
    Task(
        name: "Mã Số Sinh Viên: ",
        category: "",
        time: user.mssv,
        color: Colors.purple,
        completed: true),
    Task(
        name: "Email: ",
        category: "",
        time: user.email,
        color: Colors.lightGreenAccent,
        completed: true),
    Task(
        name: "Lớp:",
        category: "",
        time: user.nclass,
        color: Colors.pink,
        completed: false),
    Task(
        name: "Giới Tính: ",
        category: "",
        time: user.sex == "nu" ? "Nữ" : user.sex,
        color: Colors.yellowAccent,
        completed: false),
    Task(
        name: "Số Điện Thoại: ",
        category: "",
        time: user.phone,
        color: Colors.redAccent,
        completed: true),
    Task(
        name: "Khoa: ",
        category: "",
        time: user.khoaName,
        color: Colors.blueAccent,
        completed: true),
  ];
  return getTask;
}
