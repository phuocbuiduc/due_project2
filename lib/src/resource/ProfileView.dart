import 'package:due_project/src/firebase/Firebase_Auth.dart';
import 'package:due_project/src/models/Destinations/Destination.dart';
import 'package:due_project/src/models/Users.dart';
import 'package:firebase_database/firebase_database.dart';

import 'package:flutter/material.dart';

class ProfileView extends StatefulWidget {
  const ProfileView(
      {Key key, this.destination, this.auth, this.logoutCallback, this.userId})
      : super(key: key);

  final FirAuth auth;
  final VoidCallback logoutCallback;
  final String userId;

  final Destination destination;

  @override
  _ProfileViewState createState() => _ProfileViewState();
}

class _ProfileViewState extends State<ProfileView> {
  TextEditingController _textController;
  List<Users> users;
  Users user;
  DatabaseReference userRef;

  final databaseReference = FirebaseDatabase.instance.reference();

  @override
  void initState() {
    super.initState();
    _textController = TextEditingController(
      text: 'sample text: ${widget.destination.title}',
    );

    userRef = databaseReference.child('users').child(widget.userId);
  }

  void createRecord() {
    databaseReference.child("1").set({
      'title': 'Mastering EJB',
      'description': 'Programming Guide for J2EE'
    });
    databaseReference.child("2").set({
      'title': 'Flutter in Action',
      'description': 'Complete Programming Guide to learn Flutter'
    });
  }

  getData() {
    databaseReference.once().then((DataSnapshot snapshot) {
      print('Data : ${snapshot.value}');
    });
  }

  Widget _getData(DataSnapshot snapshot) {
    user = Users.fromSnapshot(snapshot);

    return Container(
      child: Column(
        children: <Widget>[
          Text("UserID: " + user.userId),
          Text("Name: " + user.name),
          Text("Giới Tính: " + user.sex),
          Text("Lớp: " + user.nclass),
          Text("Khoa: " + user.khoaName),
          Text("Mã Số SV: " + user.mssv),
          Text("Email: " + user.email),
          Text("Số DT: " + user.phone),
        ],
      ),
    );
  }

  void getDataByUsersID() {
    databaseReference.once().then((DataSnapshot snapshot) {
      print('Data : ${snapshot.value}');
    });
  }

  void updateData() {
    databaseReference
        .child('1')
        .update({'description': 'J2EE complete Reference'});
  }

  void deleteData() {
    databaseReference.child('1').remove();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Firebase Connect'),
      ),
      resizeToAvoidBottomPadding: false,
      body: Center(
          child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          RaisedButton(
            child: Text('Create Record'),
            onPressed: () {
              createRecord();
            },
          ),
          RaisedButton(
            child: Text('View Record'),
            onPressed: () {
              getData();
            },
          ),
          RaisedButton(
            child: Text('Udate Record'),
            onPressed: () {
              updateData();
            },
          ),
          RaisedButton(
            child: Text('Delete Record'),
            onPressed: () {
              deleteData();
            },
          ),
          new Container(
              alignment: Alignment.center,
              padding: const EdgeInsets.all(16.0),
              child: new FutureBuilder(
                  future: userRef.once(),
                  builder: (BuildContext context, AsyncSnapshot snapshot) {
                    if (snapshot.connectionState == ConnectionState.done &&
                        snapshot.hasData != null) {
                      return _getData(snapshot.data);
                    } else {
                      return CircularProgressIndicator();
                    }
                  }))
        ],
      )), //center
      endDrawer: Drawer(
        // Add a ListView to the drawer. This ensures the user can scroll
        // through the options in the drawer if there isn't enough vertical
        // space to fit everything.

        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: <Widget>[
            UserAccountsDrawerHeader(
              accountName: Text("Bùi Đức Phước",
                  style: TextStyle(fontSize: 18, color: Color(0xff323643))),
              accountEmail: Text(
                "Ducphuock42@gmail.com",
                style: TextStyle(fontSize: 18, color: Color(0xff323643)),
              ),
              currentAccountPicture: CircleAvatar(
                backgroundColor:
                    Theme.of(context).platform == TargetPlatform.iOS
                        ? Colors.blue
                        : Colors.white,
                child: new CircleAvatar(
                  radius: 50.0,
                  backgroundColor: const Color(0xFF778899),
                  backgroundImage: AssetImage("assets/phuoc.jpg"),
                ),
              ),
              decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("assets/bg.png"), fit: BoxFit.cover),
              ),
            ),
            ListTile(
              leading: Image.asset("ic_menu_user.png"),
              title: Text(
                "My Profile",
                style: TextStyle(fontSize: 17, color: Color(0xff323643)),
              ),
            ),
            ListTile(
              leading: Image.asset("ic_menu_history.png"),
              title: Text(
                "Reload",
                style: TextStyle(fontSize: 17, color: Color(0xff323643)),
              ),
            ),
            ListTile(
              leading: Image.asset("ic_menu_percent.png"),
              title: Text(
                "Offers",
                style: TextStyle(fontSize: 17, color: Color(0xff323643)),
              ),
            ),
            ListTile(
              leading: Image.asset("ic_menu_notify.png"),
              title: Text(
                "Notifications",
                style: TextStyle(fontSize: 17, color: Color(0xff323643)),
              ),
            ),
            ListTile(
              leading: Image.asset("ic_menu_help.png"),
              title: Text(
                "Help & Supports",
                style: TextStyle(fontSize: 17, color: Color(0xff323643)),
              ),
            ),
            ListTile(
              leading: Image.asset("assets/ic_menu_logout.png"),
              title: GestureDetector(
                child: Text(
                  "Logout",
                  style: TextStyle(fontSize: 17, color: Color(0xff323643)),
                ),
                onTap: () {
                  signOut11();
                },
              ),
            )
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    _textController.dispose();
    super.dispose();
  }

  signOut11() async {
    try {
      Navigator.of(context).pop();
      await widget.auth.signOut();
      widget.logoutCallback();
    } catch (e) {
      print(e);
    }
  }
}
