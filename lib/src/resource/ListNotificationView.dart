import 'dart:async';

import 'package:due_project/src/firebase/Firebase_UserInfo.dart';
import 'package:due_project/src/models/Notifications.dart';
import 'package:due_project/src/models/Users.dart';
import 'package:due_project/src/utils/ConvertUserToListTopics.dart';
import 'package:due_project/src/utils/IntToDateTime.dart';
import 'package:due_project/src/utils/SortListNotificationByDateTime.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:due_project/src/resource/notification/Colors.dart' as myColors;
import 'package:due_project/src/resource/notification/NotificationItems.dart';
import 'package:due_project/src/resource/dialog/Loading.dart';


class ListNotificationView extends StatefulWidget {
  final Users userFirebase;
  final String userID;

  const ListNotificationView({Key key, this.userFirebase, this.userID})
      : super(key: key);

  @override
  _ListNotificationViewState createState() => _ListNotificationViewState();
}

class _ListNotificationViewState extends State<ListNotificationView> {
  bool isLoading = true;

  Notifications notify;
  List<Notifications> notifies = [];
  DatabaseReference notifyRef;
  final databaseReference = FirebaseDatabase.instance.reference();
  List<String> listTopics = [];
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      new GlobalKey<RefreshIndicatorState>();

  Users userRenew;
  FirUser firUser = new FirUser();

  @override
  void initState() {
    super.initState();

    userRenew = widget.userFirebase;
    if (widget.userFirebase == null) {
      firUser.getCurrentUser(widget.userID).then((user) {
        userRenew = user;
        print("=====NEU USERINFO NULL TAI LIST NOTIFICATION VIEW===: " +
            user.toString());
      });
    }

    listTopics = UserToListTopic.convertToListTopic(userRenew);

    notifyRef = databaseReference.child('notifications');

    getNotifications(listTopics).then((value) {
      setState(() {
        isLoading = false;
      });
      listenerAddChid();
    });
  }

  void listenerAddChid() {
    notifies.clear();


    FirebaseDatabase.instance
        .reference()
        .child('notifications')
        .onChildAdded
        .listen((event) {



      setState(() {
        print("NOTIFIES SAU KHI CLEAN: " + notifies.toString());
        isLoading = true;
      });
      Map<dynamic, dynamic> listNotification = event.snapshot.value;
      if (listNotification != null && listNotification.length > 0) {
        listNotification.forEach((key, value) {
          if (value != null) {
            Notifications n = Notifications.fromJson(value);
            if (listTopics.indexOf(n.topic) >= 0) {
              print("GET NOTIFICATION LÚC ON ADD CHILD;  " + n.toString());
              notifies.add(n);
            }
          }
        });
      }

      setState(() {
        notifies =
            SortListNotificationByDateTime.sortListNotificationByDateTime(
                notifies);
        isLoading = false;
      });
    });
  }

  Future<List<Notifications>> getNotifications(List<String> listTopics) async {
    notifies.clear();
    await notifyRef.once().then((DataSnapshot snapshot) {
      Map<dynamic, dynamic> listNotifications = snapshot.value;
      if (listNotifications != null && listNotifications.length > 0) {
        listNotifications.forEach((key, value) {
          if (value != null) {
            value.forEach((key, value) {
              Notifications n = Notifications.fromJson(value);
              if (listTopics.indexOf(n.topic) >= 0) {
                print("GET NOTIFICATION LÚC INIT;  " + n.toString());
                notifies.add(n);
              }
            });
          }
        });
      }
    });

    //sort list Notification
    notifies =
        SortListNotificationByDateTime.sortListNotificationByDateTime(notifies);
    print("///LIST NOTIFICATIONS AFTER ADD: " + notifies.toString());
    return notifies;
  }

  Future<Null> _refresh() {
    notifies.clear();
    return getNotifications(listTopics).then((notifies1) {
      setState(() {
        notifies = notifies1;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    if (isLoading == true) {
      return Loading();
    } else {
      return Container(
        child: Scaffold(
          backgroundColor: myColors.blue,
          appBar: AppBar(
            actions: <Widget>[
              Row(
                children: <Widget>[
                  IconButton(
                    onPressed: () {},
                    icon: Stack(
                      children: <Widget>[
                        Icon(
                          Icons.notifications,
                          color: Colors.black,
                        ),
                        Positioned(
                          left: 14.0,
                          child: Icon(
                            Icons.brightness_1,
                            color: Colors.red,
                            size: 9.0,
                          ),
                        )
                      ],
                    ),
                  )
                ],
              )
            ],
            elevation: 0,
            iconTheme: IconThemeData(
              color: Colors.white,
            ),
            centerTitle: true,
            title: Text(
              'Notifications',
              style: TextStyle(color: Colors.white, fontSize: 18),
            ),
          ),
          body: Container(
            child: Container(
              decoration: BoxDecoration(
                  color: myColors.backGround,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(15.0),
                    topRight: Radius.circular(15.0),
                  )),
              child: RefreshIndicator(
                  key: _refreshIndicatorKey,
                  onRefresh: _refresh,
                  child: _myListView(context)),
            ),
          ),
        ),
      );
    }
  }

  Widget _myListView(BuildContext context) {
    return ListView.builder(
      itemCount: notifies.length,
      itemBuilder: (context, index) {
        return NotificationItem(
          hasUnreadMessage: false,
          image: AssetImage('assets/notify.png'),
          lastMessage: notifies[index].content,
          name: notifies[index].title,
          time: IntToDateTime.convertIntToDateTime(notifies[index].time),
        );
      },
    );
  }
}
