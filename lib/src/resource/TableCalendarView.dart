import 'package:flutter/material.dart';
import 'package:table_calendar/table_calendar.dart';
import 'package:due_project/src/resource/notification/Colors.dart' as myColors;

// Example holidays
final Map<DateTime, List> _holidays = {
  DateTime(2019, 1, 1): ['New Year\'s Day'],
  DateTime(2019, 1, 6): ['Epiphany'],
  DateTime(2019, 2, 14): ['Valentine\'s Day'],
  DateTime(2019, 4, 21): ['Easter Sunday'],
  DateTime(2019, 4, 22): ['Easter Monday'],
};

class TableCalendarView extends StatefulWidget {
  TableCalendarView({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _TableCalendarViewState createState() => _TableCalendarViewState();
}

class _TableCalendarViewState extends State<TableCalendarView>
    with TickerProviderStateMixin {
  Map<DateTime, List> _events;
  List _selectedEvents;
  AnimationController _animationController;
  CalendarController _calendarController;

  @override
  void initState() {
    super.initState();
    final _selectedDay = DateTime.now();

    _events = {
      _selectedDay.subtract(Duration(days: 30)): [
        '[7:00-10:00] [D201] Tin học ứng dụng',
        '[10:00-10:00] [A401] Kho và Khai phá dữ liệu',
        '[10:00-12:00] [A401] Toán Cao Cấp'
      ],
      _selectedDay.subtract(Duration(days: 27)): [
        '[10:00-12:00] [A401] Tư tưởng HCM'
      ],
      _selectedDay.subtract(Duration(days: 20)): [
        '[10:00-12:00] [A401] Nhập môn kinh doanh',
        '[10:00-12:00] [A401] Khai phá dữ liệu',
        '[10:00-12:00] [A401] Tư tưởng HCM',
      ],
      _selectedDay.subtract(Duration(days: 16)): [
        '[10:00-12:00] [A401] Tiếng anh KD3',
        '[10:00-12:00] [A401] Nhập môn kinh doanh DL3'
      ],
      _selectedDay.subtract(Duration(days: 10)): [
        '[10:00-12:00] [A401] Tư tưởng HCM',
        '[10:00-12:00] [A401] Nhập môn kinh doanh',
        '[10:00-12:00] [A401] Nhập môn kinh doanh'
      ],
      _selectedDay.subtract(Duration(days: 4)): [
        '[10:00-12:00] [A401] Tin học quản lý',
        '[10:00-12:00] [A401] Quản trị chuỗi',
        '[10:00-12:00] [A401] Phương pháp nghiên cứu KH'
      ],
      _selectedDay.subtract(Duration(days: 2)): [
        '[10:00-12:00] [A401] Quản trị kinh doanh',
        '[10:00-12:00] [A401] Marketing Căn bản',
        '[10:00-12:00] [A401] Nhập môn kinh doanh',
        '[10:00-12:00] [A401] Marketing Căn bản',
        '[10:00-12:00] [A401] Marketing Căn bản',
        '[10:00-12:00] [A401] Marketing Căn bản'
      ],
      _selectedDay: [
        '[10:00-12:00] [A401] Marketing Căn bản',
        '[10:00-12:00] [A401] Nhập môn kinh doanh DL7',
        '[10:00-12:00] [A401] Tin đại cương7',
        '[10:00-12:00] [A401] Tiếng anh KD7'
      ],
      _selectedDay.add(Duration(days: 1)): [
        '[10:00-12:00] [A401] Tiếng anh KD8',
        '[10:00-12:00] [A401] Nhập môn kinh doanh DL8',
        '[10:00-12:00] [A401] Tin đại cương8',
        '[10:00-12:00] [A401] Tiếng anh KD8'
      ],
      _selectedDay.add(Duration(days: 3)): Set.from([
        '[10:00-12:00] [A401] Tiếng anh KD9',
        '[10:00-12:00] [A401] Tiếng anh KD9',
        '[10:00-12:00] [A401] Nhập môn kinh doanh DL9'
      ]).toList(),
      _selectedDay.add(Duration(days: 7)): [
        '[10:00-12:00] [A401] Tư tưởng HCM0',
        '[10:00-12:00] [A401] Nhập môn kinh doanh DL10',
        '[10:00-12:00] [A401] Tin đại cương10'
      ],
      _selectedDay.add(Duration(days: 11)): [
        '[10:00-12:00] [A401] Tư tưởng HCM1',
        '[10:00-12:00] [A401] Nhập môn kinh doanh DL11'
      ],
      _selectedDay.add(Duration(days: 17)): [
        '[10:00-12:00] [A401] Tư tưởng HCM2',
        '[10:00-12:00] [A401] Nhập môn kinh doanh DL12',
        '[10:00-12:00] [A401] Tin đại cương12',
        '[10:00-12:00] [A401] Tiếng anh KD12'
      ],
      _selectedDay.add(Duration(days: 22)): [
        '[10:00-12:00] [A401] Tư tưởng HCM3',
        '[10:00-12:00] [A401] Nhập môn kinh doanh DL13'
      ],
      _selectedDay.add(Duration(days: 26)): [
        '[10:00-12:00] [A401] Tư tưởng HCM4',
        '[10:00-12:00] [A401] Nhập môn kinh doanh DL14',
        '[10:00-12:00] [A401] Tin đại cương14'
      ],
    };

    _selectedEvents = _events[_selectedDay] ?? [];
    _calendarController = CalendarController();

    _animationController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 400),
    );

    _animationController.forward();
  }

  @override
  void dispose() {
    _animationController.dispose();
    _calendarController.dispose();
    super.dispose();
  }

  void _onDaySelected(DateTime day, List events) {
    print('CALLBACK: _onDaySelected');
    setState(() {
      _selectedEvents = events;
    });
  }

  void _onVisibleDaysChanged(
      DateTime first, DateTime last, CalendarFormat format) {
    print('CALLBACK: _onVisibleDaysChanged');
  }

  void _onCalendarCreated(
      DateTime first, DateTime last, CalendarFormat format) {
    print('CALLBACK: _onCalendarCreated');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: myColors.blue,
        appBar: AppBar(
          actions: <Widget>[
            Row(
              children: <Widget>[
                IconButton(
                  onPressed: () {},
                  icon: Stack(
                    children: <Widget>[
                      Icon(
                        Icons.add,
                        color: Colors.black,
                      ),
                    ],
                  ),
                )
              ],
            )
          ],
          elevation: 0,
          iconTheme: IconThemeData(
            color: Colors.white,
          ),
          centerTitle: true,
          title: Text(
            'Schedule',
            style: TextStyle(color: Colors.white, fontSize: 18),
          ),
        ),
        body: Container(
          decoration: BoxDecoration(
              color: myColors.backGround,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(15.0),
                topRight: Radius.circular(15.0),
              )),
          child: Column(
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              // Switch out 2 lines below to play with TableCalendar's settings
              //-----------------------
              _buildTableCalendar(),
              const SizedBox(height: 8.0),
              Expanded(child: _buildEventList()),
            ],
          ),
        ));
  }

  // Simple TableCalendar configuration (using Styles)
  Widget _buildTableCalendar() {
    return TableCalendar(
      initialCalendarFormat: CalendarFormat.twoWeeks,
      calendarController: _calendarController,
      events: _events,
      holidays: _holidays,
      startingDayOfWeek: StartingDayOfWeek.monday,
      calendarStyle: CalendarStyle(
        selectedColor: Colors.deepOrange[400],
        todayColor: Colors.deepOrange[200],
        markersColor: Colors.brown[700],
        outsideDaysVisible: false,
      ),
      headerStyle: HeaderStyle(
        formatButtonTextStyle:
            TextStyle().copyWith(color: Colors.white, fontSize: 15.0),
        formatButtonDecoration: BoxDecoration(
          color: Colors.deepOrange[400],
          borderRadius: BorderRadius.circular(16.0),
        ),
      ),
      onDaySelected: _onDaySelected,
      onVisibleDaysChanged: _onVisibleDaysChanged,
      onCalendarCreated: _onCalendarCreated,
      builders: CalendarBuilders(
        selectedDayBuilder: (context, date, _) {
          return FadeTransition(
            opacity: Tween(begin: 0.0, end: 1.0).animate(_animationController),
            child: Container(
              margin: const EdgeInsets.all(4.0),
              padding: const EdgeInsets.only(top: 5.0, left: 6.0),
              color: Colors.deepOrange[300],
              width: 100,
              height: 100,
              child: Text(
                '${date.day}',
                style: TextStyle().copyWith(fontSize: 16.0),
              ),
            ),
          );
        },
        todayDayBuilder: (context, date, _) {
          return Container(
            margin: const EdgeInsets.all(4.0),
            padding: const EdgeInsets.only(top: 5.0, left: 6.0),
            color: Colors.amber[400],
            width: 100,
            height: 100,
            child: Text(
              '${date.day}',
              style: TextStyle().copyWith(fontSize: 16.0),
            ),
          );
        },
        markersBuilder: (context, date, events, holidays) {
          final children = <Widget>[];

          if (events.isNotEmpty) {
            children.add(
              Positioned(
                right: 1,
                bottom: 1,
                child: _buildEventsMarker(date, events),
              ),
            );
          }

          if (holidays.isNotEmpty) {
            children.add(
              Positioned(
                right: -2,
                top: -2,
                child: _buildHolidaysMarker(),
              ),
            );
          }

          return children;
        },
      ),
    );
  }

  // More advanced TableCalendar configuration (using Builders & Styles)
  Widget _buildTableCalendarWithBuilders() {
    return TableCalendar(
      locale: 'pl_PL',
      calendarController: _calendarController,
      events: _events,
      holidays: _holidays,
      initialCalendarFormat: CalendarFormat.twoWeeks,
      formatAnimation: FormatAnimation.slide,
      startingDayOfWeek: StartingDayOfWeek.sunday,
      availableGestures: AvailableGestures.all,
      availableCalendarFormats: const {
        CalendarFormat.month: '',
        CalendarFormat.week: '',
      },
      calendarStyle: CalendarStyle(
        outsideDaysVisible: false,
        weekendStyle: TextStyle().copyWith(color: Colors.blue[800]),
        holidayStyle: TextStyle().copyWith(color: Colors.blue[800]),
      ),
      daysOfWeekStyle: DaysOfWeekStyle(
        weekendStyle: TextStyle().copyWith(color: Colors.blue[600]),
      ),
      headerStyle: HeaderStyle(
        centerHeaderTitle: true,
        formatButtonVisible: false,
      ),
      builders: CalendarBuilders(
        selectedDayBuilder: (context, date, _) {
          return FadeTransition(
            opacity: Tween(begin: 0.0, end: 1.0).animate(_animationController),
            child: Container(
              margin: const EdgeInsets.all(4.0),
              padding: const EdgeInsets.only(top: 5.0, left: 6.0),
              color: Colors.deepOrange[300],
              width: 100,
              height: 100,
              child: Text(
                '${date.day}',
                style: TextStyle().copyWith(fontSize: 16.0),
              ),
            ),
          );
        },
        todayDayBuilder: (context, date, _) {
          return Container(
            margin: const EdgeInsets.all(4.0),
            padding: const EdgeInsets.only(top: 5.0, left: 6.0),
            color: Colors.amber[400],
            width: 100,
            height: 100,
            child: Text(
              '${date.day}',
              style: TextStyle().copyWith(fontSize: 16.0),
            ),
          );
        },
        markersBuilder: (context, date, events, holidays) {
          final children = <Widget>[];

          if (events.isNotEmpty) {
            children.add(
              Positioned(
                right: 1,
                bottom: 1,
                child: _buildEventsMarker(date, events),
              ),
            );
          }

          if (holidays.isNotEmpty) {
            children.add(
              Positioned(
                right: -2,
                top: -2,
                child: _buildHolidaysMarker(),
              ),
            );
          }

          return children;
        },
      ),
      onDaySelected: (date, events) {
        _onDaySelected(date, events);
        _animationController.forward(from: 0.0);
      },
      onVisibleDaysChanged: _onVisibleDaysChanged,
      onCalendarCreated: _onCalendarCreated,
    );
  }

  Widget _buildEventsMarker(DateTime date, List events) {
    return AnimatedContainer(
      duration: const Duration(milliseconds: 300),
      decoration: BoxDecoration(
        shape: BoxShape.rectangle,
        color: _calendarController.isSelected(date)
            ? Colors.brown[500]
            : _calendarController.isToday(date)
                ? Colors.brown[300]
                : Colors.blue[400],
      ),
      width: 16.0,
      height: 16.0,
      child: Center(
        child: Text(
          '${events.length}',
          style: TextStyle().copyWith(
            color: Colors.white,
            fontSize: 12.0,
          ),
        ),
      ),
    );
  }

  Widget _buildHolidaysMarker() {
    return Icon(
      Icons.add_box,
      size: 20.0,
      color: Colors.blueGrey[800],
    );
  }

  Widget _buildButtons() {
    final dateTime = _events.keys.elementAt(_events.length - 2);

    return Column(
      children: <Widget>[
        Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            RaisedButton(
              child: Text('Month'),
              onPressed: () {
                setState(() {
                  _calendarController.setCalendarFormat(CalendarFormat.month);
                });
              },
            ),
            RaisedButton(
              child: Text('2 weeks'),
              onPressed: () {
                setState(() {
                  _calendarController
                      .setCalendarFormat(CalendarFormat.twoWeeks);
                });
              },
            ),
            RaisedButton(
              child: Text('Week'),
              onPressed: () {
                setState(() {
                  _calendarController.setCalendarFormat(CalendarFormat.week);
                });
              },
            ),
          ],
        ),
        const SizedBox(height: 8.0),
        RaisedButton(
          child: Text(
              'Set day ${dateTime.day}-${dateTime.month}-${dateTime.year}'),
          onPressed: () {
            _calendarController.setSelectedDay(
              DateTime(dateTime.year, dateTime.month, dateTime.day),
              runCallback: true,
            );
          },
        ),
      ],
    );
  }

  Widget _buildEventList() {
    return ListView(
      children: _selectedEvents
          .map((event) => Container(
                decoration: BoxDecoration(
                  border: Border.all(width: 0.8),
                  borderRadius: BorderRadius.circular(12.0),
                ),
                margin:
                    const EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
                child: ListTile(
                  title: Text(event.toString()),
                  onTap: () => print('$event tapped!'),
                ),
              ))
          .toList(),
    );
  }
}
