import 'package:due_project/src/blocs/auth_bloc.dart';
import 'package:due_project/src/firebase/Firebase_Auth.dart';
import 'package:due_project/src/models/Khoa.dart';
import 'package:due_project/src/models/Users.dart';
import 'package:due_project/src/resource/LoginScreen.dart';
import 'package:due_project/src/resource/SplashScreen.dart';
import 'package:due_project/src/resource/dialog/LoadingDialog.dart';
import 'package:due_project/src/resource/dialog/MsgDialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class RegisterScreen extends StatefulWidget {
  final FirAuth auth;
  final VoidCallback loginCallback;
  final VoidCallback logoutCallback;

  const RegisterScreen(
      {Key key, this.auth, this.loginCallback, this.logoutCallback})
      : super(key: key);

  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  AuthBloc authBloc = new AuthBloc();

  TextEditingController _nameController = new TextEditingController();
  TextEditingController _emailController = new TextEditingController();
  TextEditingController _passController = new TextEditingController();
  TextEditingController _phoneController = new TextEditingController();
  TextEditingController _stcodeController = new TextEditingController();
  TextEditingController _classController = new TextEditingController();

  int _radioSex = 0;

  List<Khoa> _khoa = Khoa.getKhoa();
  List<DropdownMenuItem<Khoa>> _dropdownListKhoa;
  Khoa _selectedKhoa;

  @override
  void dispose() {
    authBloc.dispose();
    super.dispose();
  }

  @override
  void initState() {
    // TODO: implement initState
    _dropdownListKhoa = buildDropdownListKhoa(_khoa);
    _selectedKhoa = _dropdownListKhoa[0].value;
    super.initState();
  }

  List<DropdownMenuItem<Khoa>> buildDropdownListKhoa(List khoas) {
    List<DropdownMenuItem<Khoa>> items = List();
    for (Khoa khoa in khoas) {
      items.add(
        DropdownMenuItem(
          value: khoa,
          child: Text(khoa.name),
        ),
      );
    }
    return items;
  }

  onChangeDropdownItem(Khoa selectedKhoa) {
    setState(() {
      _selectedKhoa = selectedKhoa;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
              image: new ExactAssetImage('assets/bg.jpg'),
              colorFilter: ColorFilter.mode(
                  Colors.black.withOpacity(0.3), BlendMode.dstATop),
              fit: BoxFit.cover),
        ),
        padding: EdgeInsets.fromLTRB(30, 0, 30, 0),
        constraints: BoxConstraints.expand(),
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 60,
              ),
              Image.asset('Logo_kinhte.png'),
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 30, 0, 6),
                child: Text(
                  "Chào Mừng Đến Với ĐHKT",
                  style: TextStyle(fontSize: 22, color: Colors.black),
                ),
              ),
              Text(
                "Đăng ký tài khoản",
                style: TextStyle(fontSize: 16, color: Colors.indigo),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 20, 0, 20),
                child: StreamBuilder(
                    stream: authBloc.nameStream,
                    builder: (context, snapshot) => TextField(
                          controller: _nameController,
                          style: TextStyle(fontSize: 18, color: Colors.black),
                          decoration: InputDecoration(
                              errorText:
                                  snapshot.hasError ? snapshot.error : null,
                              labelText: "Họ và Tên",
                              labelStyle: TextStyle(color: Colors.black87),
                              prefixIcon: Container(
                                  width: 50, child: Image.asset("ic_user.png")),
                              border: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Colors.black87, width: 1),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(6)))),
                        )),
              ),
              Container(
                padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.black54),
                  borderRadius: BorderRadius.all(Radius.circular(5.0)),
                ),
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Image.asset("sex1.png"),
                      Padding(
                        padding: const EdgeInsets.only(left: 12.0),
                        child: new Text(
                          'Giới tính :',
                          style: new TextStyle(
                            fontWeight: FontWeight.normal,
                            color: Colors.black87,
                            fontSize: 18.0,
                          ),
                        ),
                      ),
                      new Radio(
                        value: 1,
                        groupValue: _radioSex,
                        onChanged: _handleRadioSex,
                        activeColor: Colors.black,
                      ),
                      new Text(
                        'Nam',
                        style: new TextStyle(
                            fontSize: 18.0, color: Colors.black87),
                      ),
                      new Radio(
                        value: 0,
                        groupValue: _radioSex,
                        onChanged: _handleRadioSex,
                        activeColor: Colors.black,
                      ),
                      new Text(
                        'Nữ',
                        style: new TextStyle(
                            fontSize: 18.0, color: Colors.black87),
                      ),
                    ]),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20.0),
                child: Container(
                  padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.black54),
                    borderRadius: BorderRadius.all(Radius.circular(5.0)),
                  ),
                  child: Row(
                    children: <Widget>[
                      Image.asset("khoa1.png"),
                      Padding(
                        padding: const EdgeInsets.only(left: 12.0),
                        child: Text("Khoa:",
                            style: new TextStyle(
                              fontWeight: FontWeight.normal,
                              color: Colors.black87,
                              fontSize: 18.0,
                            )),
                      ),
                      SizedBox(
                        height: 20.0,
                      ),
                      Expanded(
                        child: Container(
                          padding: const EdgeInsets.fromLTRB(20, 0, 0, 0),
                          child: DropdownButton(
                            isExpanded: true,
                            value: _selectedKhoa,
                            items: _dropdownListKhoa,
                            onChanged: onChangeDropdownItem,
                            style:
                                TextStyle(color: Colors.black, fontSize: 18.0),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 20.0,
                      ),
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20),
                child: StreamBuilder(
                    stream: authBloc.phoneStream,
                    builder: (context, snapshot) => TextField(
                          controller: _phoneController,
                          style: TextStyle(fontSize: 18, color: Colors.black),
                          decoration: InputDecoration(
                              labelText: "Số Điện Thoại",
                              labelStyle: TextStyle(color: Colors.black87),
                              errorText:
                                  snapshot.hasError ? snapshot.error : null,
                              prefixIcon: Container(
                                  width: 50,
                                  child: Image.asset("ic_phone.png")),
                              border: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Colors.black87, width: 1),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(6)))),
                        )),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20),
                child: StreamBuilder(
                    stream: authBloc.stcodeStream,
                    builder: (context, snapshot) => TextField(
                          controller: _stcodeController,
                          style: TextStyle(fontSize: 18, color: Colors.black),
                          decoration: InputDecoration(
                              labelText: "Mã Số Sinh Viên",
                              labelStyle: TextStyle(color: Colors.black87),
                              errorText:
                                  snapshot.hasError ? snapshot.error : null,
                              prefixIcon: Container(
                                  width: 15,
                                  height: 15,
                                  child: Image.asset("stcode1.png")),
                              border: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Colors.black87, width: 1),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(6)))),
                        )),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20),
                child: StreamBuilder(
                    stream: authBloc.classStream,
                    builder: (context, snapshot) => TextField(
                          controller: _classController,
                          style: TextStyle(fontSize: 18, color: Colors.black),
                          decoration: InputDecoration(
                              labelText: "Lớp: Vd/ 42K21",
                              labelStyle: TextStyle(color: Colors.black87),
                              errorText:
                                  snapshot.hasError ? snapshot.error : null,
                              prefixIcon: Container(
                                  width: 15,
                                  height: 15,
                                  child: Image.asset("class1.png")),
                              border: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Colors.black87, width: 1),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(6)))),
                        )),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 20, 0, 20),
                child: StreamBuilder(
                    stream: authBloc.emailStream,
                    builder: (context, snapshot) => TextField(
                          controller: _emailController,
                          style: TextStyle(fontSize: 18, color: Colors.black),
                          decoration: InputDecoration(
                              labelText: "Email",
                              labelStyle: TextStyle(color: Colors.black87),
                              errorText:
                                  snapshot.hasError ? snapshot.error : null,
                              prefixIcon: Container(
                                  width: 50, child: Image.asset("ic_mail.png")),
                              border: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Colors.black87, width: 1),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(6)))),
                        )),
              ),
              StreamBuilder(
                  stream: authBloc.passStream,
                  builder: (context, snapshot) => TextField(
                        controller: _passController,
                        obscureText: true,
                        style: TextStyle(fontSize: 18, color: Colors.black),
                        decoration: InputDecoration(
                            errorText:
                                snapshot.hasError ? snapshot.error : null,
                            labelText: "Mật Khẩu",
                            labelStyle: TextStyle(color: Colors.black87),
                            prefixIcon: Container(
                                width: 50, child: Image.asset("ic_lock.png")),
                            border: OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: Colors.black87, width: 1),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(6)))),
                      )),
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 30, 0, 30),
                child: SizedBox(
                  width: double.infinity,
                  height: 52,
                  child: RaisedButton(
                    onPressed: _onSignUpClicked,
                    child: Text(
                      "Đăng Ký",
                      style: TextStyle(color: Colors.white, fontSize: 18),
                    ),
                    color: Color(0xff3277D8),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(6))),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 0, 0, 40),
                child: GestureDetector(
                  behavior: HitTestBehavior.opaque,
                  onTap: onLoginNowClicked,
                  child: RichText(
                    text: TextSpan(
                        text: "Bạn đã có tài khoản? ",
                        style: TextStyle(color: Colors.black87, fontSize: 17),
                        children: <TextSpan>[
                          TextSpan(
                              text: "Đăng Nhập",
                              style: TextStyle(
                                  color: Color(0xff3277D8), fontSize: 17))
                        ]),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  _onSignUpClicked() {
    print("da click");
    //hide keyboard

    var isValid = authBloc.isValid(
        _nameController.text.trim(),
        _emailController.text.trim(),
        _passController.text.trim(),
        _phoneController.text.trim(),
        _classController.text.trim(),
        _stcodeController.text.trim());

    if (isValid) {
      //create user
      //loading dialog
      LoadingDialog.showLoadingDialog(context, "Đang tải...");
      authBloc.signUp(
          _emailController.text.trim(),
          _passController.text.trim(),
          _nameController.text.trim(),
          _phoneController.text.trim(),
          _classController.text.trim().toLowerCase(),
          _stcodeController.text.trim(),
          _selectedKhoa,
          _radioSex, () {
        // On loadding success
        LoadingDialog.hideLoadingDialog(context);
//        // go to Login Screen.

        showMsgDialog(context, "XÁC THỰC NGƯỜI DÙNG",
            "Một Email đã được gửi đến hòm thư của bạn. Vui lòng xác nhận trước khi đăng nhập!");
      }, (msg) {
        // show msg dialog.
        //Turn off Dialog when has error
        LoadingDialog.hideLoadingDialog(context);
        //show error.
        MsgDialog.showMsgDialog(context, "Đăng Ký Tài Khoản SV", msg);
      });
    }
  }

  void showMsgDialog(BuildContext context, String title, String msg) {
    showDialog(
        context: context,
        builder: (context) => AlertDialog(
              title: Text(title),
              content: Text(msg),
              actions: <Widget>[
                FlatButton(
                  child: Text("Xác nhận"),
                  onPressed: () async {
                    Navigator.pop(context);
                    await widget.auth.signOut();
                    // ignore: unnecessary_statements
                    widget.logoutCallback;
                    Navigator.of(context).pushReplacement(MaterialPageRoute(
                        builder: (context) => LoginScreen(
                              auth: widget.auth,
                              loginCallback: widget.loginCallback,
                              logoutCallback: widget.logoutCallback,
                            )));
                  },
                )
              ],
            ));
  }

  void onLoginNowClicked() {
    Navigator.of(context).pushReplacement(MaterialPageRoute(
        builder: (context) => SplashScreen(
              auth: new FirAuth(),
              userFireBase: new Users("userId", "nclass", "email", "khoaID",
                  "khoaName", "mssv", "name", "phone", "sex"),
            )));
  }

  void _handleRadioSex(int value) {
    setState(() {
      _radioSex = value;
    });
  }
}
