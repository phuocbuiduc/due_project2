import 'package:due_project/src/firebase/Firebase_Auth.dart';
import 'package:due_project/src/models/Destinations/Destination.dart';
import 'package:flutter/material.dart';

class DestinationView extends StatefulWidget {
  const DestinationView(
      {Key key, this.destination, this.auth, this.logoutCallback, this.userId})
      : super(key: key);

  final FirAuth auth;
  final VoidCallback logoutCallback;
  final String userId;

  final Destination destination;

  @override
  _DestinationViewState createState() => _DestinationViewState();
}

class _DestinationViewState extends State<DestinationView> {
  TextEditingController _textController;

  @override
  void initState() {
    super.initState();
    _textController = TextEditingController(
      text: 'sample text: ${widget.destination.title}',
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('${widget.destination.title} Text'),
        backgroundColor: widget.destination.color,
      ),
      backgroundColor: widget.destination.color[100],
      body: Container(
        padding: const EdgeInsets.all(32.0),
        alignment: Alignment.center,
        child: TextField(controller: _textController),
      ),
      endDrawer: Drawer(
        // Add a ListView to the drawer. This ensures the user can scroll
        // through the options in the drawer if there isn't enough vertical
        // space to fit everything.

        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: <Widget>[
            UserAccountsDrawerHeader(
              accountName: Text("Bùi Đức Phước",
                  style: TextStyle(fontSize: 18, color: Color(0xff323643))),
              accountEmail: Text(
                "Ducphuock42@gmail.com",
                style: TextStyle(fontSize: 18, color: Color(0xff323643)),
              ),
              currentAccountPicture: CircleAvatar(
                backgroundColor:
                    Theme.of(context).platform == TargetPlatform.iOS
                        ? Colors.blue
                        : Colors.white,
                child: new CircleAvatar(
                  radius: 50.0,
                  backgroundColor: const Color(0xFF778899),
                  backgroundImage: AssetImage("assets/phuoc.jpg"),
                ),
              ),
              decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("assets/bg.png"), fit: BoxFit.cover),
              ),
            ),
            ListTile(
              leading: Image.asset("ic_menu_user.png"),
              title: Text(
                "My Profile",
                style: TextStyle(fontSize: 17, color: Color(0xff323643)),
              ),
            ),
            ListTile(
              leading: Image.asset("ic_menu_history.png"),
              title: Text(
                "Reload",
                style: TextStyle(fontSize: 17, color: Color(0xff323643)),
              ),
            ),
            ListTile(
              leading: Image.asset("ic_menu_percent.png"),
              title: Text(
                "Offers",
                style: TextStyle(fontSize: 17, color: Color(0xff323643)),
              ),
            ),
            ListTile(
              leading: Image.asset("ic_menu_notify.png"),
              title: Text(
                "Notifications",
                style: TextStyle(fontSize: 17, color: Color(0xff323643)),
              ),
            ),
            ListTile(
              leading: Image.asset("ic_menu_help.png"),
              title: Text(
                "Help & Supports",
                style: TextStyle(fontSize: 17, color: Color(0xff323643)),
              ),
            ),
            ListTile(
              leading: Image.asset("assets/ic_menu_logout.png"),
              title: GestureDetector(
                child: Text(
                  "Logout",
                  style: TextStyle(fontSize: 17, color: Color(0xff323643)),
                ),
                onTap: () {
                  signOut11();
                },
              ),
            )
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    _textController.dispose();
    super.dispose();
  }

  signOut11() async {
    try {
      Navigator.of(context).pop();
      await widget.auth.signOut();
      widget.logoutCallback();
    } catch (e) {
      print(e);
    }
  }
}
