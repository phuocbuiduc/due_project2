import 'package:due_project/src/blocs/login_bloc.dart';
import 'package:due_project/src/blocs/reset_email_bloc.dart';
import 'package:due_project/src/firebase/Firebase_Auth.dart';
import 'package:due_project/src/models/UserFirebase.dart';

import 'package:due_project/src/resource/RegisterScreen.dart';
import 'package:due_project/src/resource/SplashScreen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'dialog/LoadingDialog.dart';
import 'dialog/MsgDialog.dart';

class LoginScreen extends StatefulWidget {
  LoginScreen({this.auth, this.loginCallback, this.logoutCallback});

  final FirAuth auth;
  final VoidCallback loginCallback;
  final VoidCallback logoutCallback;

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  LoginBloc loginBloc = new LoginBloc();

  bool _showPass = false;
  bool _isSavePass = false;
  TextEditingController _userController = new TextEditingController();
  TextEditingController _passController = new TextEditingController();

  TextEditingController _resetPassController = TextEditingController();

  ResetEmailBloc resetEmailBloc = new ResetEmailBloc();

  _displayDialog(BuildContext context) async {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('Vui lòng nhập địa chỉ Email cần Khôi phục mật khẩu'),
            content: StreamBuilder(
                stream: resetEmailBloc.resetEmailStream,
                builder: (context, snapshot) => TextField(
                      style: TextStyle(fontSize: 18, color: Colors.black),
                      controller: _resetPassController,
                      decoration: InputDecoration(
                          errorText: snapshot.hasError ? snapshot.error : null,
                          labelText: "Địa chỉ Email:",
                          labelStyle:
                              TextStyle(color: Colors.black87, fontSize: 15)),
                    )),
            actions: <Widget>[
              new FlatButton(
                child: new Text('Hủy Bỏ'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
              new FlatButton(
                child: new Text('Khôi Phục'),
                onPressed: () {
                  onResetPassword().then((isReset){
                    if(isReset == true){
                      Navigator.of(context).pop();
                      MsgDialog.showMsgDialog(context, "KHÔI PHỤC MẬT KHẨU",
                          "Vui lòng truy cập liên kết trong email để đặt lại mật khẩu của bạn.");
                    }else{
                      MsgDialog.showMsgDialog(context, "Has Error", "Đã có lỗi sảy ra!.");
                    }
                  });



                },
              )
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
      body: Container(
          constraints: BoxConstraints.expand(),
          decoration: BoxDecoration(
            image: DecorationImage(
                image: new ExactAssetImage('assets/bg1.jpg'),
                colorFilter: ColorFilter.mode(
                    Colors.black.withOpacity(0.4), BlendMode.dstATop),
                fit: BoxFit.cover),
          ),
          padding: EdgeInsets.fromLTRB(30, 30, 30, 20),
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 20, 0, 10),
                  child: Container(
                      width: 140,
                      height: 140,
                      padding: EdgeInsets.all(15),
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: Color(0xffd8d8d8),
                      ),
                      child: Image.asset(
                        'logo_kt.png',
                        fit: BoxFit.cover,
                      )),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 0, 0, 20),
                  child: Text(
                    "Chào Mừng\n Sinh Viên Kinh Tế ĐN",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.blueGrey,
                        fontSize: 28),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 0, 0, 30),
                  child: StreamBuilder(
                      stream: loginBloc.userStream,
                      builder: (context, snapshot) => TextField(
                            style: TextStyle(fontSize: 18, color: Colors.black),
                            controller: _userController,
                            decoration: InputDecoration(
                                errorText:
                                    snapshot.hasError ? snapshot.error : null,
                                labelText: "EMAIL",
                                labelStyle: TextStyle(
                                    color: Colors.black87, fontSize: 15)),
                          )),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 0, 0, 10),
                  child: Stack(
                    alignment: AlignmentDirectional.centerEnd,
                    children: <Widget>[
                      StreamBuilder(
                        stream: loginBloc.passStream,
                        builder: (context, snapshot) => TextField(
                          style: TextStyle(fontSize: 18, color: Colors.black),
                          controller: _passController,
                          obscureText: !_showPass,
                          decoration: InputDecoration(
                              errorText:
                                  snapshot.hasError ? snapshot.error : null,
                              labelText: "MẬT KHẨU",
                              labelStyle: TextStyle(
                                  color: Colors.black87, fontSize: 15)),
                        ),
                      ),
                      GestureDetector(
                        behavior: HitTestBehavior.opaque,
                        onTap: onToggleShowPass,
                        child: Text(
                          _showPass ? "ẨN" : "HIỂN THỊ",
                          style: TextStyle(color: Colors.blue, fontSize: 15),
                        ),
                      )
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                  child: Container(
                      child: Row(
                    children: <Widget>[
                      Checkbox(
                          value: _isSavePass, onChanged: _handleRadioSavePass),
                      Text(
                        "LƯU MẬT KHẨU",
                        style: TextStyle(fontSize: 15, color: Colors.black87),
                      ),
                    ],
                  )),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 20, 0, 0),
                  child: SizedBox(
                    width: double.infinity,
                    height: 56,
                    child: RaisedButton(
                      onPressed: onSignInClicked,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(8))),
                      color: Colors.blue,
                      child: Text(
                        "ĐĂNG NHẬP",
                        style: TextStyle(color: Colors.white, fontSize: 16),
                      ),
                    ),
                  ),
                ),
                Container(
                  height: 130,
                  width: double.infinity,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      GestureDetector(
                        behavior: HitTestBehavior.opaque,
                        onTap: onSignUpClicked,
                        child: Text(
                          "NEW USER? ĐĂNG KÝ",
                          style: TextStyle(fontSize: 15, color: Colors.black87),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          _displayDialog(context);
                          // widget.auth.resetPassword("ducphuock42@gmail.com");
                        },
                        child: Text(
                          "KHÔI PHỤC MẬT KHẨU",
                          style: TextStyle(fontSize: 15, color: Colors.blue),
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          )),
    ));
  }

  Future<bool> onResetPassword() async {
    bool reset = false;
    String email = _resetPassController.text.trim();
    if (resetEmailBloc.isValidInfo(email)) {
      reset = true;
      await resetEmailBloc.resetPassword(widget.auth, email);
    }
    return reset;
  }

  void onSignInClicked() async {
    //String userId = "";
    //setState(()  async {
    if (loginBloc.isValidInfo(_userController.text, _passController.text)) {
      //loading dialog
      LoadingDialog.showLoadingDialog(context, "Đang tải...");
      await loginBloc
          .signIn(widget.auth, _userController.text, _passController.text, () {
        // On loadding success
        LoadingDialog.hideLoadingDialog(context);

        widget.auth.isEmailVerified().then((verifyEmail) {
          if (verifyEmail == false) {
            showMsgDialog(context, "XÁC THỰC NGƯỜI DÙNG",
                "Vui lòng nhấp vào liên kết vừa được gửi đến tài khoản email của bạn để xác minh email của bạn.");
          } else {
            widget.auth.getCurrentUser().then((user) async {
              Navigator.of(context).pushReplacement(MaterialPageRoute(
                  builder: (context) => UserFirebase(userID: user.uid, loginCallback: widget.loginCallback,auth: widget.auth,)));
            });
            print("Nếu đã verify rồi thì tiến hành subcribe.");
          }
        });
      }, (msg) {
        // show msg dialog.
        //Turn off Dialog when has error
        LoadingDialog.hideLoadingDialog(context);
        //show error.
        MsgDialog.showMsgDialog(context, "ĐĂNG NHẬP DUE APP", msg);
      });
    }
    //});
  }

  void showMsgDialog(BuildContext context, String title, String msg) {
    showDialog(
        context: context,
        builder: (context) => AlertDialog(
              title: Text(title),
              content: Text(msg),
              actions: <Widget>[
                FlatButton(
                  child: Text("OK"),
                  onPressed: () async {
                    Navigator.pop(context);
                    await widget.auth.sendEmailVerification();
                    widget.auth.signOut();
                    // ignore: unnecessary_statements
                    widget.logoutCallback;

                    Navigator.of(context).pushReplacement(MaterialPageRoute(
                        builder: (context) => LoginScreen(
                              auth: widget.auth,
                              loginCallback: widget.loginCallback,
                              logoutCallback: widget.logoutCallback,
                            )));
                  },
                )
              ],
            ));
  }

  void onSignUpClicked() {
    Navigator.of(context).pushReplacement(MaterialPageRoute(
        builder: (context) => RegisterScreen(
              auth: widget.auth,
              logoutCallback: widget.logoutCallback,
              loginCallback: widget.loginCallback,
            )));
  }

  void onToggleShowPass() {
    setState(() {
      _showPass = !_showPass;
    });
  }

  void _handleRadioSavePass(bool value) {
    setState(() {
      _isSavePass = value;
    });
  }
}
