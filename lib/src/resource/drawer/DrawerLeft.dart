import 'package:due_project/src/firebase/Firebase_Auth.dart';
import 'package:due_project/src/firebase/Firebase_UserInfo.dart';
import 'package:due_project/src/models/Users.dart';
import 'package:due_project/src/utils/ConvertUserToListTopics.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';

class DrawerLeft extends StatelessWidget {
  DrawerLeft(
      {Key key, this.auth, this.logoutCallback, this.userId, this.userFirebase})
      : super(key: key);

  final FirAuth auth;
  final VoidCallback logoutCallback;
  final String userId;
  final Users userFirebase;

  FirUser firUser = new FirUser();

  final FirebaseMessaging _fcm = FirebaseMessaging();

  unSubribedTopics() async {


    List<String> listTopics = UserToListTopic.convertToListTopic(userFirebase);

    listTopics.forEach((topics) {
      _fcm.unsubscribeFromTopic(topics).then((done) {
        print("=====UNSUBRIBED TOPICS BEFORE LOGOUT===/// " + topics);
      });
    });
  }

  @override
  Widget build(BuildContext context) {

    signOut11() async {
      print("USER AT LOGOUT: " + userFirebase.toString());
      unSubribedTopics();
      try {
        Navigator.of(context).pop();
        await auth.signOut();
        logoutCallback();
      } catch (e) {
        print(e);
      }
    }

    return Drawer(
      // Add a ListView to the drawer. This ensures the user can scroll
      // through the options in the drawer if there isn't enough vertical
      // space to fit everything.

      child: ListView(
        // Important: Remove any padding from the ListView.
        padding: EdgeInsets.zero,
        children: <Widget>[
          UserAccountsDrawerHeader(
            accountName: Text(userFirebase.name,
                style: TextStyle(fontSize: 18, color: Color(0xff323643))),
            accountEmail: Text(
              userFirebase.email,
              style: TextStyle(fontSize: 18, color: Color(0xff323643)),
            ),
            currentAccountPicture: CircleAvatar(
              backgroundColor: Theme.of(context).platform == TargetPlatform.iOS
                  ? Colors.blue
                  : Colors.white,
              child: new CircleAvatar(
                radius: 50.0,
                backgroundColor: const Color(0xFF778899),
                backgroundImage: AssetImage("assets/phuoc.jpg"),
              ),
            ),
            decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("assets/bg.png"), fit: BoxFit.cover),
            ),
          ),
          ListTile(
            leading: Image.asset("ic_menu_user.png"),
            title: Text(
              "My Profile",
              style: TextStyle(fontSize: 17, color: Color(0xff323643)),
            ),
          ),
          ListTile(
            leading: Image.asset("ic_menu_history.png"),
            title: Text(
              "Thông báo nghỉ, ốm",
              style: TextStyle(fontSize: 17, color: Color(0xff323643)),
            ),
          ),
          ListTile(
            leading: Image.asset("ic_menu_percent.png"),
            title: Text(
              "Xem Điểm",
              style: TextStyle(fontSize: 17, color: Color(0xff323643)),
            ),
          ),
          ListTile(
            leading: Image.asset("ic_menu_notify.png"),
            title: Text(
              "Notifications",
              style: TextStyle(fontSize: 17, color: Color(0xff323643)),
            ),
          ),
          ListTile(
            leading: Image.asset("ic_menu_help.png"),
            title: Text(
              "Help & Supports",
              style: TextStyle(fontSize: 17, color: Color(0xff323643)),
            ),
          ),
          ListTile(
            leading: Image.asset("assets/ic_menu_logout.png"),
            title: GestureDetector(
              child: Text(
                "Đăng Xuất",
                style: TextStyle(fontSize: 17, color: Color(0xff323643)),
              ),
              onTap: () {
                signOut11();
              },
            ),
          )
        ],
      ),
    );
  }
}
